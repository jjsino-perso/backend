class FeaturesController < ApplicationController
  include Response
  before_action :find_feature, only: [:show, :update, :destroy]

  def index
      @features = Feature.all
      json_response(@features)
  end
  
  def create
    @feature = Feature.create!(feature_params)
    json_response(@feature, :created)
  end
  
  def show
    json_response(@feature)
  end

  def update
    if @feature.nil?
      head 404
    else
      @feature.update(feature_params)
      head :no_content
    end
  end

  def destroy
    if @feature.nil?
      head 404
    else
      @feature.destroy
      head :no_content
    end
  end

  private

  def feature_params
    params.permit(:name)
  end

  def find_feature
    @feature = Feature.find(params[:id])
  end
end
