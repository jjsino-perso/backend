class TestsController < ApplicationController
    include Response
    before_action :find_feature
    before_action :find_test, only: [:show, :update, :destroy]
    
    # GET /features/:feature_id/tests
    def index
        json_response(@feature.tests)
    end

    # GET /features/:feature_id/tests/:id
    def show
        json_response(@test)
    end

    # POST /features/:feature_id/tests
    def create
        @feature.tests.create!(test_params)
        json_response(@feature, :created)
    end

    # PUT /features/:feature_id/tests/:id
    def update
        @test.update(test_params)
        head :no_content
    end

    # DELETE /features/:feature_id/tests/:id
    def destroy
        @test.destroy
        head :no_content
    end

    private

    def test_params
        params.permit(:name, :status)
    end

    def find_feature
        @feature = Feature.find(params[:feature_id])
    end

    def find_test
        @test = @feature.tests.find_by!(id: params[:id]) 
        if @feature
        end
    end
end
