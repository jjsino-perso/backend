class Feature
  include Mongoid::Document

  field :name, type: String
  has_many :tests, class_name: "Test", inverse_of: :feature

  before_save :default_values
  def default_values
    self.tests ||= []
  end

  validates_presence_of :name
end
