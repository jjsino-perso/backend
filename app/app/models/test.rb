class Test
  include Mongoid::Document
  field :name, type: String
  field :status, type: String # { undefined: 'undefined', passed: 'passed', failed: 'failed' }
  belongs_to :feature, class_name: "Feature", inverse_of: :tests, optional: true

  validates_presence_of :name

  before_save :default_values
  def default_values
    self.status ||= 'undefined'
  end
end
